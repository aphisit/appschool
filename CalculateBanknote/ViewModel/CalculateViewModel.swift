//
//  CalculateViewModel.swift
//  CalculateBanknote
//
//  Created by Mac on 30/3/2564 BE.
//

import UIKit
protocol CalculateViewModelDelegate:class {
    func successData(_ controller: CalculateViewModel, text: String, bankArray:Array<Int>)
}
class CalculateViewModel: NSObject {
    weak var delegate:CalculateViewModelDelegate?
    var x1000:Int, x500:Int, x100:Int, x50:Int, x20:Int, x10:Int, x5:Int
    var bankArray:Array<Int>
    
    
    override init() {
        x1000 = 0
        x500 = 0
        x100 = 0
        x50 = 0
        x20 = 0
        x10 = 0
        x5 = 0
        bankArray = []
        
    }
    func calculate(price:Int,typeBank:Int) -> Void {
    
        x1000 = Int(price / 1000)
        x500 = Int(price % 1000)/500
        x100 = Int(price % 500)/100
        x50 = Int(price % 100)/50
        x20 = Int(price % 50)/20
        x10 = Int(price % 20)/10
        x5 = Int(price % 10)/5
        
        bankArray.append(x1000)
        bankArray.append(x500)
        bankArray.append(x100)
        bankArray.append(x50)
        bankArray.append(x20)
        bankArray.append(x10)
        bankArray.append(x5)
        
        delegate?.successData(self, text: "YES",bankArray: bankArray)
        print("xx")
        
    }

}
;
