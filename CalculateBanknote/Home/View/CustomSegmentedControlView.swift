//
//  CustomSegmentedControlView.swift
//  CalculateBanknote
//
//  Created by Mac on 25/5/2564 BE.
//

import UIKit

protocol CustomSegmentedControlViewDelegate:class {
    func changToindax(index:Int)
}

class CustomSegmentedControlView: UIView {
    
    weak var delegate:CustomSegmentedControlViewDelegate?
    static var customSegment = CustomSegmentedControlView()
    
    
    private var buttonTitles:[String]!
    private var buttons:[UIButton] = []
    private var selectorView:UIView!
    
    var textColor:UIColor = .black
    var selectorViewColor:UIColor = .red
    var selectorTextColor:UIColor = .red
    
    convenience init (frame:CGRect, buttonTitle:[String]){
        self.init(frame:frame)
        self.buttonTitles = buttonTitle
        
        
    }
    
    private func configStackView(){
        let stack = UIStackView(arrangedSubviews: buttons)
        stack.axis = .horizontal
        stack.alignment = .fill
        stack.distribution = .fillEqually
        addSubview(stack)
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        stack.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        stack.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        stack.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
    }
    
    private func configSelectorView(){
        let selectorWidth = frame.width / CGFloat(self.buttonTitles.count)
        selectorView = UIView(frame: CGRect(x: 0, y: self.frame.height, width: selectorWidth, height: 2))
        selectorView.backgroundColor = selectorViewColor
        addSubview(selectorView)
    }
        
    private func createButton(){
        buttons = [UIButton]()
        buttons.removeAll()
        subviews.forEach({$0.removeFromSuperview()})
        for buttonTitle in buttonTitles {
            let button = UIButton(type: .system)
            button.setTitle(buttonTitle, for: .normal)
            button.addTarget(self, action: #selector(CustomSegmentedControlView.buttonAction(sender:)), for: .touchUpInside)
            button.setTitleColor(textColor, for: .normal)
            buttons.append(button)
        }
        buttons[0].setTitleColor(selectorTextColor, for: .normal)
    }
    
    @objc func buttonAction(sender:UIButton){
        for (buttonIndex,btn) in buttons.enumerated() {
            btn.setTitleColor(textColor, for: .normal)
            if btn == sender {
                let selectorPosition = frame.width/CGFloat(buttonTitles.count) * CGFloat(buttonIndex)
                delegate?.changToindax(index: buttonIndex)
                UIView.animate(withDuration: 0.3){
                    self.selectorView.frame.origin.x = selectorPosition
                }
                btn.setTitleColor(selectorTextColor, for: .normal)
            }
        }
    }
    
    private func updateView(){
        createButton()
        configSelectorView()
        configStackView()
        delegate?.changToindax(index: 0)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        updateView()
    }
    
    func setButtonTitles(buttonTitles:[String]) {
        self.buttonTitles = buttonTitles
        updateView()
    }
    
}
