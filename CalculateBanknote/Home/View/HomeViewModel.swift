//
//  HomeViewModel.swift
//  CalculateBanknote
//
//  Created by Mac on 9/6/2564 BE.
//

import UIKit

class HomeViewModel {
    
    let attendanceVC:AttendanceViewControllerXIB = AttendanceViewControllerXIB()
    let infoVC:InfoViewControllerXIB = InfoViewControllerXIB()
    
    
    
    
    func showViewOfControSagement(targetView:UIView, index:Int, height:Int) -> Void {
        attendanceVC.view.frame = CGRect(x: targetView.frame.origin.x, y: CGFloat(height)+2, width: targetView.frame.size.width, height: targetView.frame.size.height-CGFloat(height))
        
        infoVC.view.frame = CGRect(x: targetView.frame.origin.x , y:CGFloat(height)+2, width: targetView.frame.size.width, height: targetView.frame.size.height - CGFloat(height))
        
        if index == 0 {
            targetView.addSubview(attendanceVC.view)
        }
        else {
            targetView.addSubview(infoVC.view)
        }
        
       
       
        print("xxx")
    }
}
